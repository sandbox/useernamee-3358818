Hook Profiler
===

Tracks the execution time of hooks per module.

It outputs the logs into a csv file in the /tmp folder.

This module is very rudimentary. At first I tried using the watchdog to log
execution times but there was a circular dependency issue (some logger channels
are dependent on config.factory and config.factory is dependent on
module_handler). Second plan that failed was to use file.system for path
resolution but I was faced with the same error there.

There is massive room for improvement of this module and I'm open for PRs.

But if you just need a quick (and dirty) way to check how much time each of
your hooks takes for execution, this module will provide it.
