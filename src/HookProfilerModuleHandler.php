<?php

namespace Drupal\hook_profiler;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * The handler class to log hook execution and invocation.
 */
class HookProfilerModuleHandler extends ModuleHandler {

  /**
   * Original module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The log path string.
   *
   * @var string
   */
  protected string $logPath;

  /**
   * Constructs a decorated ModuleHandler object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Original module handler service.
   * @param string $root
   *   The app root.
   * @param array $module_list
   *   An associative array whose keys are the names of installed modules and
   *   whose values are Extension class parameters. This is normally the
   *   %container.modules% parameter being set up by DrupalKernel.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend for storing module hook implementation information.
   *
   * @see \Drupal\Core\DrupalKernel
   * @see \Drupal\Core\CoreServiceProvider
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, $root, array $module_list, CacheBackendInterface $cache_backend) {
    $this->moduleHandler = $moduleHandler;
    $this->logPath = getenv('HOOK_PROFILER_LOG_PATH') ?? '/tmp/hook_profiler.log';
    parent::__construct($root, $module_list, $cache_backend);
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAll($hook, array $args = []) {
    $return = [];
    $hook_name = $hook;
    $this->invokeAllWith($hook, function (callable $hook, string $module) use ($args, &$return, $hook_name) {
      $start = microtime(TRUE);
      $result = call_user_func_array($hook, $args);
      if (isset($result) && is_array($result)) {
        $return = NestedArray::mergeDeep($return, $result);
      }
      elseif (isset($result)) {
        $return[] = $result;
      }
      $exution_time = microtime(TRUE) - $start;
      $this->log($start, $hook_name, $module, $exution_time);
    });
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($module, $hook, array $args = []) {
    if (!$this->hasImplementations($hook, $module)) {
      return;
    }
    $start = microtime(TRUE);
    $hookInvoker = \Closure::fromCallable($module . '_' . $hook);
    $return = call_user_func_array($hookInvoker, $args);
    $exution_time = microtime(TRUE) - $start;
    $this->log($start, $hook, $module, $exution_time);
    return $return;
  }

  /**
   * Writes hook execution time into log file.
   *
   * @param float $start
   *   Timestamp of hook execution start.
   * @param string $hook
   *   Hook name.
   * @param string $module
   *   Module name that implements the hook.
   * @param float $time
   *   Hook execution time.
   */
  protected function log($start, $hook, $module, $time) {
    $log = "$start, $hook, $module, $time" . PHP_EOL;
    file_put_contents("/tmp/hook_profiler_log.txt", $log, FILE_APPEND | LOCK_EX);
  }

}
